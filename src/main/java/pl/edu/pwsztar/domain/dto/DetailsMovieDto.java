package pl.edu.pwsztar.domain.dto;


public class DetailsMovieDto {
    private String title;
    private String videoId;
    private String image;
    private Integer year;

    public DetailsMovieDto() {
    }

    private DetailsMovieDto(Builder builder) {
        title = builder.title;
        videoId = builder.videoId;
        image = builder.image;
        year = builder.year;
    }

    public String getTitle() {
        return title;
    }

    public String getVideoId() {
        return videoId;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public static final class Builder {
        private String title;
        private String videoId;
        private String image;
        private Integer year;

        public Builder() {
        }

        public DetailsMovieDto.Builder title(String title) {
            this.title = title;
            return this;
        }

        public DetailsMovieDto.Builder videoId(String videoId) {
            this.videoId = videoId;
            return this;
        }

        public DetailsMovieDto.Builder image(String image) {
            this.image = image;
            return this;
        }

        public DetailsMovieDto.Builder year(Integer year) {
            this.year = year;
            return this;
        }


        public DetailsMovieDto build() {
            return new DetailsMovieDto(this);
        }
    }

    @Override
    public String toString() {
        return "DetailsMovieDto{" +
                "title='" + title + '\'' +
                ", videoId='" + videoId + '\'' +
                '}';
    }
}
