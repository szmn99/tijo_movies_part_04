package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private final long counter;

    public MovieCounterDto(long counter) {
        this.counter = counter;
    }

    private MovieCounterDto(Builder builder) {
        counter = builder.counter;
    }

    public long getCounter() {
        return counter;
    }

    public static final class Builder {
        private long counter;

        public Builder() {
        }

        public MovieCounterDto.Builder title(Long counter) {
            this.counter = counter;
            return this;
        }

        public MovieCounterDto build() {
            return new MovieCounterDto(this);
        }
    }

    @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }
}
